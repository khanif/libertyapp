(function () {

    function log(msg, type) {
        type = type || "log";
        switch (type) {
            case "log":
            default:
                console.log(msg);
                break;
            case "info":
                console.info(msg);
                break;
        }
    }

    var defaultSettings = {
        theme: "./styles/bootstrap/superhero/bootstrap.css",
        nav: "navbar-inverse"
    }

    function getState() {
        var data = localStorage.getItem("libState");
        if (data) {
            return JSON.parse(data);
        }

        return defaultSettings;
    }

    function setState(data) {
        data = data || {};
        var current = getState();
        for (var key in current) {
            var newField = data[key]; 
            if (newField) {
                current[key] = newField;                
            }             
        }
        
        localStorage.setItem("libState", JSON.stringify(current));        
    }
    
    function stylePath (name) {
        return "./styles/bootstrap/" + name +"/bootstrap.css";
    }
    
    function setStyle(name) {
        var current = getState(); 
        current.theme = stylePath(name);
        $("#libertyStyle").attr("href", current.theme);
        setState(current);
    }
    
    function setHeaderClass(className) {
        var current = getState(); 
        current.nav = className; 
        setState(current);
    }
    
    function toggleNavbar() {
        $("#navbar-main").dblclick(function () {
            var header = $(".navbar-fixed-top"); 
            defaultHeader = "navbar-default";
            inverseHeader = "navbar-inverse";
            
            if(header.hasClass(defaultHeader)) {
                header.removeClass(defaultHeader);
                header.addClass(inverseHeader);
                setHeaderClass(inverseHeader);
            } else {
                header.removeClass(inverseHeader);
                header.addClass(defaultHeader);
                setHeaderClass(defaultHeader);
            }
            
        });
    }
    
    var state = getState();    
    $("#libertyStyle").attr("href", state.theme);
    $(document).ready(function () {
        $('body').css("display", "block");
        toggleNavbar();
        
        $("#btnThemes li").click(function (evt) {
           var theme = evt.target.text.toLowerCase().replace(" ","");
           setStyle(theme); 
        });
    })
        
    log("Libery core is loaded -:- ");
})();